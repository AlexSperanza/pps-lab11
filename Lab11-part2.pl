% fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):-fromList([H2|T],L).

% fromCircList(+List, -Graph)
fromCircList([H|[]], Graph) :- fromCircListWithFirst([H|[]], H, Graph).
fromCircList([H1,H2|T],[e(H1,H2)|L]):-fromCircListWithFirst([H2|T], H1, L).
fromCircListWithFirst([H|[]], First, [e(H, First)]).
fromCircListWithFirst([H1,H2|T], First, [e(H1,H2)|L]) :- fromCircListWithFirst([H2|T], First, L).

% dropAll -> see Part1
dropAll(X, [], []) :- !.
dropAll(X, [X2|T], O) :- copy_term(X, X2), dropAll(X, T, O), !.
dropAll(X, [H|T], [H|O]) :- X\==H, dropAll(X, T, O).

% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
dropNode(G,N,O):- dropAll(e(N,_),G,G2), dropAll(e(_,N),G2,O).

% reaching(+Graph, +Node, -List).
reaching(G, N, L) :- findall(Y, member(e(N,Y), G), O1), findall(X, member(e(X,N), G), O2), append(O1, O2, L).

% anypath(+Graph, +Node1, +Node2, -ListPath)
anypath(G, N1, N2, L) :- member(e(N1, N2), G), L = [e(N1, N2)].
anypath(G, N1, N2, L) :- member(e(N1, X), G), anypath(G, X, N2, L2), L = [e(N1, X)|L2].

% allreaching(+Graph, +Node, -List)
allreaching(G, N, O) :- findall(Y, anypath(G, N, Y, L), O1), findall(X, anypath(G, X, N, L), O2), append(O1, O2, O).
